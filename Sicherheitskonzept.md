Man kann Daten aber auch lokal, z.B. in einem Textdokument speichern, dafür muss man die Daten herauslesen und hineinschreiben. 
Dies kann man nicht wirklich direkt mit HTML machen, sondern man muss JavaScript oder PHP nutzen. 
Dies macht die ganze Situation noch komplizierter, jedoch nicht unmöglich. 
Aus zeitlichen Gründen verzichten wir auf diese Implementierung, da wir bereits die Daten auf einer SQL-Datenbank speichern. 
Man könnte einen Code nehmen, welcher Ähnlich zu diesem Aussieht: (Um den Javascript-Code zu sehen, bitte auf "edit" drücken)
 <script type ="text/javascript">
 function WriteToFile(passForm) {

    set fso = CreateObject("Scripting.FileSystemObject");  
    set s = fso.CreateTextFile("C:\test.txt", True);
    s.writeline(document.passForm.input1.value);
    s.writeline(document.passForm.input2.value);
    s.writeline(document.passForm.input3.value);
    s.Close();
 }
  </script>

Oder mit ActiveX:
<script type = "text/javascript">
function WriteToFile(passForm)
{
var fso = new ActiveXObject("Scripting.FileSystemObject");
var s = fso.CreateTextFile("C:\\Test.txt", true);
s.WriteLine(document.passForm.input.value);
s.Close();
}
</script> 

Natürlich ist das kein 1:1 funktionierender Code, jedoch müsste es so ähnlich aussehen (einfacht gesagt).


Für die Transaktion haben wir ein Template erstellt, welches unserer Meinung nach sehr gelungen ist. 
Um Paypal einzubinden, müssten wir jedoch bei ihnen um eine API fragen, was zu viel Aufwand und Zeit kostet und keine Garantie ist. 
Es gibt jedoch einen Spende-Button, welchen wir implementieren könnten. 
Dies haben wir nicht getan, da es gegen die Nutzungsbedingungen spricht, da unsere Website sich nicht um Spenden dreht. 
Man muss auf https://www.paypal.com/donate/buttons gehen und sich mit einem Paypal-Konto anmelden, welches wir nicht haben, um den Code für den Button zu bekommen, welchen wir in die Website implementieren könnten.


Das Login konnten wir leider nicht perfekt umsetzen.
Wir konnten registrieren, den Account in SQL speichern und das Passwort hashen, jedoch ging das Überprüfen nicht, ob ein User bereits existiert. 
Es ist ein kleiner Fehler eingeschlichen, welchen wir nicht finden konnten. Es war sehr zeitaufwendig und daher lassen wir es so, wie es ist. 
Es sollte trotzdem im Code ersichtlich sein, dass wir es sehr versucht haben. Man kann das Login umgehen, indem man auf den Button «zum Shop» drückt. 
Normalerweise ginge das nicht, aber da das Login nicht geht, muss ich dies so machen.




Reflexionen:

Raul: Das Projekt war sehr Anstrengend und Zeitaufwendig aber es hat trotzdem Spass gemacht. Ich habe viel gelernt und mich auch weiter mit Spring und html auseinandergesetzt, was sehr positiv ist. Mein Job war das Programmieren, was auch einigermassen gut erledigt wurde. Ich bekam auch unterstützung von meiner Gruppe. Die Zeit war leider sehr eng aber trotzdem bin ich mit dem Resultat zufrieden.

Dennis: Mir hat das Auseinandersetzen mit diesem Projekt Spass gemacht, da die Wichtigkeit von Schutzmassnahmen bei E-Commerce-Anwendungen sehr interessant ist und einen hohen Stellenwert hat. Ich durfte die Seite mit den Produkten gestalten, die ich mit tollen Dingen verziert habe(Button mit Preis, Hover, Checkbox). Da wir nicht die ganze Anwendung umsetzten und auch nicht mussten, haben wir uns dafür entschieden schon ausgewählte Produkte für den „Kauf“ auszuwählen(locked Checkbox), um den Kaufprozess einfach zu verdeutlichen. 
Zudem habe ich herrn Munoz bei der Implementierung der Registrierung und des Logins unterstützt, indem ich ihm beim hashen von Passwörtern geholfen habe.

Domenico: Dieses Projekt hat mir sehr gut gefallen, auch wenn wir erst spät anfangen konnten. Raul und ich waren anfangs ein bisschen verloren, als dann aber Dennis dazugekommen ist, hatten wir erst recht Motivation gefunden, eine «anständige» Arbeit zu machen. Wir konnten einige Vorlagen von vorherigen Projekten übernehmen und unsere neuen Ideen implementieren. Die Zusammenarbeit von mir und Raul ist schon seit Anfang der IMS optimal, daher konnten wir, sobald wir eine «Vision» hatten, diese schnell umsetzen, und Dennis konnte dann sein eigenes Flair einbringen. Ich konnte Ideen und Konzepte suchen, Produkte samt Bildern und Beschreibung übernehmen, und das Team ein bisschen organisieren. Ich würde immer wieder mit Dennis und Raul ein Projekt dieser Art machen.
